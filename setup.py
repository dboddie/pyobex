#! /usr/bin/env python3

import setuptools

from PyOBEX import __version__

DESCRIPTION = ("A package implementing aspects "
               "of the Object Exchange (OBEX) protocol.")

with open("README.rst", encoding="utf-8") as file:
    long_description = file.read()

setuptools.setup(
    name="PyOBEX",
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type="text/x-rst",
    author="David Boddie",
    author_email="david@boddie.org.uk",
    url="https://gitlab.com/dboddie/pyobex",
    version=__version__,
    license="GPL version 3 (or later)",
    platforms="Cross-platform",
    packages=["PyOBEX"],
    install_requires=["PyBluez >= 0.23"],
    project_urls={
        "Bug Reports": "https://gitlab.com/dboddie/pyobex/-/issues",
        "Source": "https://gitlab.com/dboddie/pyobex",
    },
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "Intended Audience :: Science/Research",
        ("License :: OSI Approved :: GNU General Public License v3 "
         "or later (GPLv3+)"),
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3.12",
        "Programming Language :: Python :: Implementation",
    ]
)
