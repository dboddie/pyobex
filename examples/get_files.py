#!/usr/bin/env python3

import os
import sys
from xml.etree import ElementTree

from PyOBEX import client, responses

if __name__ == "__main__":

    if len(sys.argv) != 4:

        sys.stderr.write(
            f"Usage: {sys.argv[0]} <device address> <port> <directory>\n")
        sys.exit(1)

    device_address = sys.argv[1]
    port = int(sys.argv[2])
    path = sys.argv[3]

    c = client.BrowserClient(device_address, port)

    response = c.connect()
    if not isinstance(response, responses.ConnectSuccess):
        sys.stderr.write("Failed to connect.\n")
        sys.exit(1)

    pieces = path.split("/")

    for piece in pieces:

        response = c.setpath(piece)
        if isinstance(response, responses.FailureResponse):
            sys.stderr.write("Failed to enter directory.\n")
            sys.exit(1)

    sys.stdout.write(f"Entered directory: {path}\n")

    response = c.listdir()

    if isinstance(response, responses.FailureResponse):
        sys.stderr.write("Failed to list directory.\n")
        sys.exit(1)

    headers, data = response
    tree = ElementTree.fromstring(data)
    for element in tree.findall("file"):

        name = element.attrib["name"]

        if os.path.exists(name):
            sys.stderr.write(f"File already exists: {name}\n")
            continue

        sys.stdout.write(f"Fetching file: {name}\n")

        response = c.get(name)

        if isinstance(response, responses.FailureResponse):
            sys.stderr.write(f"Failed to get file: {name}\n")
        else:
            sys.stdout.write(f"Writing file: {name}\n")
            headers, data = response
            try:
                with open(name, "wb") as file:
                    file.write(data)
            except IOError:
                sys.stderr.write(f"Failed to write file: {name}\n")

    c.disconnect()
    sys.exit()
