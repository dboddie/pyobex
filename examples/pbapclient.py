#!/usr/bin/env python3

import sys
from xml.etree import ElementTree

import bluetooth

from PyOBEX import client, headers, responses

if __name__ == "__main__":

    if not 2 <= len(sys.argv) <= 3:
        sys.stderr.write(f"Usage: {sys.argv[0]} <device address> [SIM]\n")
        sys.exit(1)

    elif len(sys.argv) == 3:
        if sys.argv[2] == "SIM":
            # If the SIM command line option was given, look in the SIM1
            # directory. Maybe the SIM2 directory exists on dual-SIM phones.
            PREFIX = "SIM1/"
        else:
            sys.stderr.write(
                f"Usage: {sys.argv[0]} <device address> [SIM]\n")
            sys.exit(1)
    else:
        PREFIX = ""

    device_address = sys.argv[1]

    d = bluetooth.find_service(address=device_address, uuid="1130")
    if not d:
        sys.stderr.write("No Phonebook service found.\n")
        sys.exit(1)

    port = d[0]["port"]

    # Use the generic Client class to connect to the phone.
    c = client.Client(device_address, port)
    UUID = b"\x79\x61\x35\xf0\xf0\xc5\x11\xd8\x09\x66\x08\x00\x20\x0c\x9a\x66"
    result = c.connect(header_list=[headers.Target(UUID)])

    if not isinstance(result, responses.ConnectSuccess):
        sys.stderr.write("Failed to connect to phone.\n")
        sys.exit(1)

    # Access the list of vcards in the phone's internal phone book.
    hdrs, cards = c.get(PREFIX + "telecom/pb",
                        header_list=[headers.Type(b"x-bt/vcard-listing")])

    # Parse the XML response to the previous request.
    root = ElementTree.fromstring(cards)

    print(f"\nAvailable cards in {PREFIX}telecom/pb\n")

    # Examine each XML element, storing the file names we find in a list, and
    # printing out the file names and their corresponding contact names.
    names = []
    for card in root.findall("card"):
        print(f"{card.attrib['handle']}: {card.attrib['name']}")
        names.append(card.attrib["handle"])

    print(f"\nCards in {PREFIX}telecom/pb\n")

    # Request all the file names obtained earlier.
    c.setpath(f"{PREFIX}telecom/pb")

    for name in names:
        hdrs, card = c.get(name, header_list=[headers.Type(b"x-bt/vcard")])
        print(card)

    # Return to the root directory.
    c.setpath(to_parent=True)
    c.setpath(to_parent=True)
    if PREFIX:
        c.setpath(to_parent=True)

    print(f"\nThe phonebook in {PREFIX}telecom/pb as one vcard\n")

    hdrs, phonebook = c.get(f"{PREFIX}telecom/pb.vcf",
                            header_list=[headers.Type(b"x-bt/phonebook")])
    print(phonebook)

    c.disconnect()

    sys.exit()
